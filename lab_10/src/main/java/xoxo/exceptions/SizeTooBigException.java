package xoxo.exceptions;

/**
 * An exception that is thrown if the message size is
 * larger than 10Kbit.
 *
 * @author Jeremia Delviero Hutagalung
 */
public class SizeTooBigException extends Exception {

    //TODO: Implement the exception
    public SizeTooBigException(String message) {
        super(message);
    }
}