package xoxo.exceptions;

/**
 * An exception that is thrown if the Kiss Key that
 * is used to encrypt a message contains other
 * characters than A-Z, a-z, and @.
 *
 * @author Jeremia Delviero Hutagalung
 */
public class InvalidCharacterException extends Exception {

    //TODO: Implement the exception
    public InvalidCharacterException(String message) {
        super(message);
    }

}