package xoxo.exceptions;

/**
 * An exception that is thrown if the Seed that
 * is used to encrypt a message is not in range
 * 0 - 36 (inclusive).
 *
 * @author Jeremia Delviero Hutagalung
 */
public class RangeExceededException extends Exception {

    //TODO: Implement the exception
    public RangeExceededException(String message) {
        super(message);
    }

}