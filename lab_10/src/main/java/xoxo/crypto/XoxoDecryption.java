package xoxo.crypto;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key
     * string to build the Hug Key object.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }


    /**
     * Decrypts an encrypted message.
     *
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decryptMessage(String encryptedMessage, int seed) {
        //TODO: Implement decryption algorithm
        String decryptedMessage = "";
        for (int i = 0; i < encryptedMessage.length(); i++) {
            int m = hugKeyString.charAt(i % hugKeyString.length()) ^ seed;
            int k = m - 'a';
            int value = encryptedMessage.charAt(i) ^ k;
            decryptedMessage += (char) value;
        }
        return decryptedMessage;
    }

}