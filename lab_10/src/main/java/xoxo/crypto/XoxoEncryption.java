package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     *
     * @throws KeyTooLongException       if the length of the
     *                                   kissKeyString exceeded 28 characters.
     * @throws InvalidCharacterException if kissKeyString
     *                                   other character than A-Z, a-z, and @.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException,
            InvalidCharacterException {
        if (kissKeyString.length() > 28) {
            throw new KeyTooLongException("Length of the Key exceeded 28 characters.");
        }
        for (int i = 0; i < kissKeyString.length(); i++) {
            int c = kissKeyString.charAt(i);
            if (c < 64 || c > 90 && c < 97 || c > 122) {
                throw new InvalidCharacterException("Key contain other character than A-Z, a-z, and @");
            }
        }
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     *
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     * and a Hug Key object that can be used to decrypt the message.
     * @throws SizeTooBigException if message size is greater than 1 Kbit / 128 Bytes.
     */
    public XoxoMessage encrypt(String message) throws SizeTooBigException {
        String encryptedMessage = this.encryptMessage(message);
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     *
     * @param message The message that wants to be encrypted.
     * @param seed    A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     * and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if seed is not in range 0-36 (inclusive).
     * @throws SizeTooBigException    if message size is greater than 1Kbit / 128 Bytes.
     */
    public XoxoMessage encrypt(String message, int seed) throws RangeExceededException,
            SizeTooBigException {
        //TODO: throw RangeExceededException for seed requirements
        if (seed < 0 || seed > 36) {
            throw new RangeExceededException("Seed is not in range 0-36 (inclusive)");
        }
        String encryptedMessage = this.encryptMessage(message);
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     *
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if message size is greater than 1 Kbit / 128 Bytes.
     */
    private String encryptMessage(String message) throws SizeTooBigException {
        //TODO: throw SizeTooBigException for message requirements
        final int length = message.length();
        if (length > 128) {
            throw new SizeTooBigException("Message size is greater than 1 Kbit");
        }
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}

