package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.util.XoxoMessage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static xoxo.key.HugKey.DEFAULT_SEED;

/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Jeremia Delviero Hutagalung
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    private int encryptedFileId = 0;
    private int decryptedFileId = 0;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
        gui.setDecryptFunction(e -> decrypt());
        gui.setEncryptFunction(e -> encrypt());
    }

    //TODO: Create any methods that you want

    /**
     * Method untuk mengenkripsi message yang diinput
     * beserta kisskey dan seednya dan ditulis kedalam
     * file berekstensi .enc.
     */
    public void encrypt() {
        String message = gui.getMessageText();
        String kissKey = gui.getKeyText();
        String seed = gui.getSeedText();
        XoxoEncryption encryptor;
        try {
            encryptor = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.setWarning("Key length must not exceed 28 characters");
            return;
        } catch (InvalidCharacterException e) {
            gui.setWarning("Key can contain only A-Z, a-z, and @");
            return;
        }
        XoxoMessage encryptedMessage;
        try {
            if (seed.equals("")) {
                encryptedMessage = encryptor.encrypt(message);
                seed = "DEFAULT_SEED";
            } else {
                encryptedMessage = encryptor.encrypt(message, Integer.parseInt(seed));
            }
        } catch (RangeExceededException e) {
            gui.setWarning("Seed must be in range 0 - 36 (inclusive)");
            return;
        } catch (SizeTooBigException e) {
            gui.setWarning("Message size is larger than 10 Kbit");
            return;
        } catch (NumberFormatException e) {
            gui.setWarning("Seed must be integer");
            return;
        } catch (NullPointerException e) {
            return;
        }
        writeEncryptedMessage(encryptedMessage, seed);
    }

    /**
     * Method untuk mendekripsi message yang diinput
     * beserta hugkey dan seednya dan ditulis kedalam
     * file berekstensi .txt.
     */
    public void decrypt() {
        String encryptedMessage = gui.getMessageText();
        String hugKeyString = gui.getKeyText();
        int seed;
        if (gui.getSeedText().equals("")) {
            seed = DEFAULT_SEED;
        } else {
            seed = Integer.parseInt(gui.getSeedText());
        }
        XoxoDecryption decryptor = new XoxoDecryption(hugKeyString);
        String decryptedMessage = decryptor.decryptMessage(encryptedMessage, seed);
        writeDecryptedMessage(decryptedMessage);
    }

    /**
     * Method untuk memasukkan hasil enkripsi kedalam file
     * berekstensi .enc.
     *
     * @param encryptedMessage berisi String dan hugkey yang dapat digunakan untuk
     *                         mendekripsi kembali pesan yang terenkripsi
     * @param seed             angka yang dipakai untuk men-generate hugkey
     */
    public void writeEncryptedMessage(XoxoMessage encryptedMessage, String seed) {
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\encrypted files\\encrypted"
                + encryptedFileId++ + ".enc");
        try {
            FileWriter writer = new FileWriter(file, false);
            writer.write(encryptedMessage.getEncryptedMessage());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            gui.appendLog("Encrypt failed! Cannot create file.");
        }
        gui.appendLog("Encrypted text: " + encryptedMessage.getEncryptedMessage());
        gui.appendLog("Hug key: " + encryptedMessage.getHugKey().getKeyString());
        gui.appendLog("Seed: " + seed);
    }

    /**
     * Method untuk memasukkan hasil dekripsi kedalam file
     * berekstensi .txt.
     *
     * @param decryptedMessage String hasil pesan yang didekripsi
     */
    public void writeDecryptedMessage(String decryptedMessage) {
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\decrypted files\\decrypted"
                + decryptedFileId++ + ".txt");
        try {
            FileWriter writer = new FileWriter(file, false);
            writer.write(decryptedMessage);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            gui.appendLog("Decrypt failed! Cannot create file.");
        }
        gui.appendLog("Decrypted text: " + decryptedMessage);
    }

}