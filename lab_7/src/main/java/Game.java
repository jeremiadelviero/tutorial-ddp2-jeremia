import character.*;

import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<String> dietInfo = new ArrayList<String>();

    /**
     * Fungsi untuk mencari karakter
     *
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (int i = 0; i < player.size(); i++) {
            if (player.get(i).getName().equals(name)) {
                return player.get(i);
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (find(chara) == null) {
            Player temp = null;
            if (tipe.equals("Human")) {
                temp = new Human(chara, hp);
            } else if (tipe.equals("Monster")) {
                temp = new Monster(chara, hp);
            } else if (tipe.equals("Magician")) {
                temp = new Magician(chara, hp);
            }
            player.add(temp);
            return "=======================================\n"
                    + chara + " ditambah ke game";
        }
        return "=======================================\nSudah ada karakter bernama "
                + chara;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (find(chara) == null) {
            Player temp = null;
            if (tipe.equals("Human")) {
                temp = new Human(chara, hp);
            } else if (tipe.equals("Monster")) {
                temp = new Monster(chara, hp, roar);
            } else if (tipe.equals("Magician")) {
                temp = new Magician(chara, hp);
            }
            player.add(temp);
            return "=======================================\n"
                    + chara + " ditambah ke game";
        }
        return "=======================================\nSudah ada karakter bernama "
                + chara;
    }

    /**
     * fungsi untuk menghapus character dari game
     *
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        Player willRemoved = find(chara);
        if (willRemoved != null) {
            player.remove(willRemoved);
            return "=======================================\n"
                    + chara + " dihapus dari game";
        }
        return "=======================================\nTidak ada "
                + chara;
    }


    /**
     * fungsi untuk menampilkan status character dari game
     *
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        Player temp = find(chara);
        String status = "";
        if (temp != null) {
            status += temp.getTipe() + " " + temp.getName() + "\nHP : " + temp.getHp();
            if (temp.isDead()) {
                status += "\nSudah meninggal dunia dengan damai\n";
            } else {
                status += "\nMasih hidup\n";
            }
            if (temp.getDiet().size() == 0) {
                status += "Belum memakan siapa siapa";
            } else {
                status += "Memakan ";
                for (int i = 0; i < temp.getDiet().size(); i++) {
                    status += temp.getDiet().get(i);
                    if (i < temp.getDiet().size() - 1) {
                        status += ", ";
                    }
                }
            }
            return "=======================================\n"
                    + status;
        }
        return "=======================================\nTidak ada "
                + chara;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     *
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {
        String status = "";
        if (player != null) {
            for (Player character : player) {
                status += status(character.getName()) + "\n";
            }
            return status;
        }
        return "=======================================\nTidak ada character";
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     *
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        String diet = "";
        Player temp = find(chara);
        if (temp != null) {
            if (temp.getDiet().size() != 0) {
                for (String eaten : temp.getDiet()) {
                    diet += eaten;
                }
                return "=======================================\n"
                        + diet;
            } else {
                return "=======================================\nBelum memakan siapa siapa";
            }
        }
        return "=======================================\nTidak ada"
                + chara;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String eaten = "";
        if (dietInfo != null) {
            for (int i = 0; i < dietInfo.size(); i++) {
                eaten += dietInfo.get(i);
                if (i < dietInfo.size() - 1) {
                    eaten += ", ";
                }
            }
            return "=======================================\n"
                    + eaten;
        }
        return "=======================================\nBelum ada player yang dimakan";
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        if (attacker != null && attacked != null) {
            return "=======================================\n"
                    + attacker.attack(attacked);
        }
        return "=======================================\nTidak ada "
                + meName + " atau " + enemyName;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player burner = find(meName);
        Player burnt = find(enemyName);
        if (burner != null && burnt != null) {
            if (burner.getTipe().equals("Magician")) {
                return "=======================================\n"
                        + ((Magician) burner).burn(burnt);
            } else {
                return "=======================================\n"
                        + burner.getName() + " tidak bisa membakar " + burnt.getName();
            }
        }
        return "=======================================\nTidak ada "
                + meName + " atau " + enemyName;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        Player eater = find(meName);
        Player eaten = find(enemyName);
        if (eater != null && eaten != null) {
            if (eater.canEat(eaten)) {
                player.remove(eaten);
                dietInfo.add(eaten.getTipe() + " " + eaten.getName());
            }
            return "=======================================\n"
                    + eater.eat(eaten);
        }
        return "=======================================\nTidak ada "
                + meName + " atau " + enemyName;
    }


    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     *
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        Player whoRoar = find(meName);
        if (whoRoar != null) {
            if (whoRoar.getTipe().equals("Monster")) {
                return "=======================================\n"
                        + ((Monster) whoRoar).roar();
            } else {
                return "=======================================\n"
                        + whoRoar.getName() + " tidak bisa berteriak";
            }
        }
        return "=======================================\nTidak ada "
                + meName;
    }

    // Fungsi cetakMenu untuk soal bonus
    // Mencetak attribute diet untuk setiap player yang masih ada di game
    public String cetakMenu () {
        String output = "";
        for (Player chara : player) {
            output += "---" + chara.getName() + "\n";
            output += cetakTree(chara) + "\n";
        }
        return output;
    } 

    public String cetakTree(Player chara) {
        String output = "";
        if (chara.getDietEaten().size() == 0 || chara.getDietEaten() == null) {
            return "";
        } else {
            for (Player eaten : chara.getDietEaten()) {
                output += "---" + eaten.getName();
            }
            output += "\n";
            for (Player eaten : chara.getDietEaten()) {
                output += cetakTree(eaten);   
            }
        }
        return output;
    }

}