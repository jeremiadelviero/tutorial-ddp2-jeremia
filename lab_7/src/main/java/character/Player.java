package character;

import java.util.ArrayList;

public class Player{
    private String name;
    private int hp;
    private String tipe;
    private ArrayList<String> diet = new ArrayList<String>();
    private ArrayList<Player> dietEaten = new ArrayList<Player>();
    private boolean isDead = false;
    private boolean isBurnt = false;

    public Player(String name, int hp, String tipe) {
        this.name = name;
        this.hp = hp;
        this.tipe = tipe;
        if (this.hp <= 0) {
            this.isDead = true;
            this.hp = 0;
        }
    }

    public String attack(Player enemy) {
        if (!this.isDead) {
            if (enemy.getTipe().equals("Magician")) {
                enemy.setHp(enemy.getHp() - 10 * 2);
            } else {
                enemy.setHp(enemy.getHp() - 10);
            }
            if (enemy.getHp() <= 0) {
                enemy.setHp(0);
                enemy.setDead(true);
            }
            return "Nyawa " + enemy.getName() + " " + enemy.getHp();
        }
        return this.name + " tidak bisa menyerang " + enemy.getName();
    }

    public String eat(Player enemy) {
        if (this.canEat(enemy)) {
            this.hp += 15;
            this.diet.add(enemy.getTipe() + " " + enemy.getName() + " ");
            this.dietEaten.add(enemy);
            return this.name + " memakan " + enemy.getName() + "\n"
                    + "Nyawa " + this.name + " kini " + this.hp;
        }
        return this.name + " tidak bisa memakan " + enemy.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public ArrayList<String> getDiet() {
        return diet;
    }

    public void setDiet(ArrayList<String> diet) {
        this.diet = diet;
    }

    public ArrayList<Player> getDietEaten() {
        return dietEaten;
    }

    public void setDietEaten(ArrayList<Player> dietEaten) {
        this.dietEaten = dietEaten;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public boolean isBurnt() {
        return isBurnt;
    }

    public void setBurnt(boolean burnt) {
        isBurnt = burnt;
    }

    public boolean canEat(Player enemy) {
        if (this.tipe.equals("Magician") || this.tipe.equals("Human")) {
            if (enemy.isBurnt() && enemy.getTipe().equals("Monster")) {
                return true;
            }
        } else if (this.tipe.equals("Monster")) {
            if (enemy.isDead()) {
                return true;
            }
        }
        return false;
    }
}
