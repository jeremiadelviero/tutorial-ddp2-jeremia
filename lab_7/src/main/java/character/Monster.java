package character;

public class Monster extends Player {
    private String roarSound = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster (String name, int hp) {
        super(name, hp * 2, "Monster");
    }

    public Monster (String name, int hp, String roarSound) {
        super(name, hp * 2, "Monster");
        this.roarSound = roarSound;
    }

    public String roar() {
        return roarSound;
    }
}