package character;

public class Human extends Player {

    public Human (String name, int hp) {
        super(name, hp, "Human");
    }
}