package character;

public class Magician extends Player {

    public Magician(String name, int hp) {
        super(name, hp, "Magician");
    }

    public String burn(Player enemy) {
        if (!this.isDead()) {
            if (enemy.getTipe().equals("Magician")) {
                enemy.setHp(enemy.getHp() - 10 * 2);
            } else {
                enemy.setHp(enemy.getHp() - 10);
            }
            if (enemy.getHp() <= 0) {
                enemy.setHp(0);
                enemy.setDead(true);
                enemy.setBurnt(true);
                return "Nyawa " + enemy.getName() + " " + enemy.getHp() + "\ndan matang";
            } else {
                return "Nyawa " + enemy.getName() + " " + enemy.getHp();
            }
        } else {
            return this.getName() + " tidak bisa membakar " + enemy.getName();
        }
    }
}