/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	private String nama;
	private boolean sudahRestart = false;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}
	
	public BingoCard(Number[][] numbers, Number[] numberStates, String nama) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
		this.nama = nama;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		//TODO Implement
		if (this.numberStates[num] != null) {
			if (!this.numberStates[num].isChecked()) {
				this.numberStates[num].setChecked(true);
				if (this.nama != null) {
					this.checkWin();
					return this.nama + ": " + num + " tersilang";
				} else {
					this.checkWin();
					return num + " tersilang";
				}
			} else {
				if (this.nama != null) {
					return this.nama + ": " + num + " sebelumnya sudah tersilang";
				} else {
					return num + " sebelumnya sudah tersilang";
				}
			} 
		}
		if (this.nama != null) {
			return this.nama + ": Kartu tidak memiliki angka " + num;
		}
		return "Kartu tidak memiliki angka " + num;
	}
	
	public void checkWin() {
		if (this.numbers[0][0].isChecked() && this.numbers[1][1].isChecked() && this.numbers[2][2].isChecked() &&
			this.numbers[3][3].isChecked() && this.numbers[4][4].isChecked()) {
			this.setBingo(true);
		} else if ((this.numbers[0][4].isChecked() && this.numbers[1][3].isChecked() && this.numbers[2][2].isChecked() &&
					this.numbers[3][1].isChecked() && this.numbers[4][0].isChecked())) {
			this.setBingo(true);
		} else {
			for (int i = 0; i < 5; i++) {
				if (this.numbers[i][0].isChecked() && this.numbers[i][1].isChecked() && this.numbers[i][2].isChecked() &&
					this.numbers[i][3].isChecked() && this.numbers[i][4].isChecked()) {
					this.setBingo(true);	
				} else if (this.numbers[0][i].isChecked() && this.numbers[1][i].isChecked() && this.numbers[2][i].isChecked() &&
						   this.numbers[3][i].isChecked() && this.numbers[4][i].isChecked()) {
					this.setBingo(true);
				}
			}
		}
		if (this.isBingo()) {
			System.out.println("BINGO!");
			System.out.println(this.info());
			System.exit(0);
		}
	}
	
	public String info() {
		//TODO Implement
		String info = "";
		if (this.nama != null) {
			info = this.nama + "\n"; 
		}
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (this.numbers[i][j].isChecked()) {
					info += "| X  ";
				} else {
					info += "| " + this.numbers[i][j].getValue() + " ";
				}
			}
			if (i != 4) {
				info += "|\n";
			} else {
				info += "|";
			}
		}
		return info;
	}
	
	public void restart(){
		//TODO Implement
		if (!this.sudahRestart) {
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					this.numbers[i][j].setChecked(false);
				}
			}
			this.sudahRestart = true;
			System.out.println("Mulligan!");
		} else {
			if (this.nama != null) {
				System.out.println(this.nama + "sudah pernah mengajukan RESTART");
			} else {
				System.out.println("sudah pernah mengajukan RESTART");
			}
		}
	}
}
