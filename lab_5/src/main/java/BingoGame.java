import java.util.HashMap;
import java.util.Scanner;

public class BingoGame {
	
	private static HashMap<String, BingoCard> paraPemainStates = new HashMap<String, BingoCard>();
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String[] numberInput = new String[5];
		boolean flag = false;
		String[] command;
		
		String[] inputPemain = input.nextLine().split(" ");
		int jumlahPemain = Integer.parseInt(inputPemain[0]);
		
		BingoCard[] paraPemain = new BingoCard[10];
		
		for (int i = 0; i < jumlahPemain; i++) {	
			Number[][] initNumber = new Number[5][5];
			Number[] initNumberStates = new Number[100];
			paraPemain[i] = new BingoCard(initNumber , initNumberStates, inputPemain[i+1]);
			paraPemainStates.put(inputPemain[i+1], paraPemain[i]);
		
		
			for (int j = 0; j < 5; j++) {
				numberInput = (input.nextLine()).split(" ");
				for (int k = 0; k < 5; k++) {
					initNumber[j][k] = new Number(Integer.parseInt(numberInput[k]), j, k);
					initNumberStates[initNumber[j][k].getValue()] = initNumber[j][k];
					paraPemain[i].setNumbers(initNumber);
					paraPemain[i].setNumberStates(initNumberStates);
				}
			}
		}		
		flag = true;
		
		while (flag) {
			command = input.nextLine().split(" ");
			if (command[0].toUpperCase().equals("MARK")) {
				for (int i = 0; i < jumlahPemain; i++) {
					BingoCard pemain = paraPemain[i];
					System.out.println(pemain.markNum(Integer.parseInt(command[1])));
				}
			} else if (command[0].toUpperCase().equals("INFO")) {
				System.out.println(paraPemainStates.get(command[1]).info());
			} else if (command[0].toUpperCase().equals("RESTART")) {
				paraPemainStates.get(command[1]).restart();
			} else {
				System.out.println("Incorrect command");
			}			
		}
	}
}