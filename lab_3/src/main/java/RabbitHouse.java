import java.util.Scanner;

// @author Jeremia Delviero Hutagalung, NPM 1706039875, Kelas B, GitLab Account: @jeremiadelviero

public class RabbitHouse{
	// Membuat fungsi rekursif soal wajib
	public static int normal(int panjangNama) {
		// Base Case
		if (panjangNama == 1) {
			return 1;
		// Recursive Case
		} else {
			return normal(panjangNama - 1) * panjangNama + 1;
		}
	}
		
	// Membuat fungsi rekursif soal bonus
	public static int palindrom(String nama) {
		// Mendeklarasikan variabel yang akan dipakai di dalam loop
		int hasil = 0;
		int hasilTemp = 0;
		// Membuat variable yang berisi string reverse dari input
		String reverse = new StringBuilder(nama).reverse().toString();
		// Base Case jika panjang string <= 1
		if (nama.length() <= 1) {
			hasil += 0;
		// Base Case jika string input palindrom
		} else if (nama.equals(reverse)) {
			hasil += 0;
		// Recursive Case
		} else {
			for (int i = 0; i < nama.length(); i++) {
				hasilTemp += palindrom(nama.substring(0,i) + nama.substring(i+1,nama.length()));
			}
			hasil += hasilTemp + 1;
		}
		return hasil;
	}	
	
	// Membuat main method
	public static void main(String[] args) {	
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);
		
		// Menerima input perintah dan nama dari user
		String perintah = input.next().toLowerCase();
		String nama = input.next().toUpperCase();
		int panjangNama = nama.length();
		
		// Melaksanakan perintah dan mengeluarkan output
		// sesuai input dari user
		if (perintah.equals("normal")) {
			System.out.println(normal(panjangNama));
		} else if (perintah.equals("palindrom")) {
			System.out.println(palindrom(nama));
		}
		
		// Menutup input setelah selesai dipakai
		input.close();
	}
}
