import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Jeremia Delviero Hutagalung, NPM 1706039875, Kelas B, GitLab Account: @jeremiadelviero
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = Integer.parseInt(input.nextLine());
		if (panjang <= 0 || panjang > 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = Integer.parseInt(input.nextLine());
		if (lebar <= 0 || lebar > 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = Integer.parseInt(input.nextLine());
		if (tinggi <= 0 || tinggi > 250){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine());
		if (berat <= 0 || berat > 150){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		int makanan = Integer.parseInt(input.nextLine());
		if (makanan <= 0 || makanan > 20){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = Integer.parseInt(input.nextLine());


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		float rasioFloat = (berat) / (((float)panjang / 100) * ((float)lebar / 100) * ((float)tinggi / 100));
		int rasio = (int) rasioFloat;

		for (int i = 0; i < jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + (i + 1) + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			String catatanPrint = "";
			if (catatan.isEmpty()){
				catatanPrint = "Tidak ada catatan tambahan";
			} else {
				catatanPrint = "Catatan: " + catatan;
			}

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima + "\n" +
						   "--------------------\n" +
						   nama + " - " + alamat + "\n" +
						   "Lahir pada tanggal " + tanggalLahir + "\n" + 
						   "Rasio Berat Per Volume     = " + rasio + " kg/m^3\n" +
						   catatanPrint; 
			System.out.println(hasil);
		}

		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int panjangNama = nama.length();
		int asciiJumlah = 0;
		
		for (int i = 0; i < panjangNama; i++){
			char huruf = nama.charAt(i);
			int asciiHuruf = huruf;
			asciiJumlah = asciiJumlah + asciiHuruf;
		}
		int kalkulasi = ((panjang * tinggi * lebar) + asciiJumlah) % 10000;
		


		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0) + String.valueOf(kalkulasi);

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (50000 * 365) * (makanan);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String[] tanggal = tanggalLahir.split("-"); // lihat hint jika bingung
		int tahunLahir = Integer.parseInt(tanggal[2]);
		int umur = (2018) - (tahunLahir);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String apartemen = "";
		String kabupaten = "";
		if (umur >= 0 && umur <= 18) {
			apartemen = "PPMT";
			kabupaten = "Rotunda";
		} else if(umur >= 19 && umur <= 1018){
			if (anggaran >= 0 && anggaran <= 100000000){
				apartemen = "Teksas";
				kabupaten = "Sastra";
			} else if (anggaran >= 100000000){
				apartemen = "Mares";
				kabupaten = "Margonda";
			}
		}



		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "\nREKOMENDASI APARTEMEN\n" +
							 "--------------------\n" +
							 "MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga + "\n" +
							 "MENIMBANG:  Anggaran makanan tahunan: Rp " + anggaran + "\n" +
							 "            Umur kepala keluarga: " + umur + " tahun\n" +
							 "MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" +
							 apartemen + ", kabupaten " + kabupaten;
		System.out.println(rekomendasi);
		

		input.close();
	}
}
