package lab9.event;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * A class representing an event and its properties
 */
public class Event implements Comparable<Event> {
    /**
     * Name of event
     */
    private String name;

    // TODO: Make instance variables for representing beginning and end time of event
    private GregorianCalendar startEvent;
    private GregorianCalendar endEvent;

    // TODO: Make instance variable for cost per hour
    private BigInteger cost;

    // TODO: Create constructor for Event class
    public Event(String name, GregorianCalendar startEvent, GregorianCalendar endEvent, BigInteger cost) {
        this.name = name;
        this.startEvent = startEvent;
        this.endEvent = endEvent;
        this.cost = cost;
    }

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for cost field.
     *
     * @return cost of this event instance
     */
    public BigInteger getCost() {
        return this.cost;
    }

    // TODO: Implement toString()
    public String toString() {
        String toString = String.format("%s\n"
                        + "Waktu mulai: %02d-%02d-%02d, %02d:%02d:%02d\n"
                        + "Waktu selesai: %02d-%02d-%02d, %02d:%02d:%02d\n"
                        + "Biaya kehadiran: ", name, startEvent.get(Calendar.DAY_OF_MONTH),
                (startEvent.get(Calendar.MONTH) + 1), startEvent.get(Calendar.YEAR),
                startEvent.get(Calendar.HOUR_OF_DAY), startEvent.get(Calendar.MINUTE),
                startEvent.get(Calendar.SECOND), endEvent.get(Calendar.DAY_OF_MONTH),
                (endEvent.get(Calendar.MONTH) + 1), endEvent.get(Calendar.YEAR),
                endEvent.get(Calendar.HOUR_OF_DAY), endEvent.get(Calendar.MINUTE),
                endEvent.get(Calendar.SECOND)) + cost;
        return toString;
    }

    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
    public boolean overlapsWith(Event other) {
        if (startEvent.compareTo(other.startEvent) == 0
                && endEvent.compareTo(other.endEvent) == 0) {
            return true;
        } else if (startEvent.compareTo(other.startEvent) > 0 && startEvent.compareTo(other.endEvent) < 0) {
            return true;
        } else if (endEvent.compareTo(other.startEvent) > 0 && endEvent.compareTo(other.endEvent) < 0) {
            return true;
        } else if (startEvent.compareTo(other.startEvent) < 0 && endEvent.compareTo(other.endEvent) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Method yang digunakan untuk membuat clone agar tidak mengarah ke objek yang sama
     *
     * @return instance Event yang telah di-clone
     */
    public Event clone() {
        Event eventClone = new Event(this.name, (GregorianCalendar) this.startEvent.clone(),
                (GregorianCalendar) this.endEvent.clone(), this.cost);
        return eventClone;
    }

    /**
     * Method yang digunakan agar dapat mengurutkan Event dalam suatu List berdasarkan waktu startEvent nya
     *
     * @param event Instance lain yang akan dicompare
     * @return compareTo antara instance startEvent yang ada didalam this dan startEvent yang ada di
     * instance event pada argumen
     */
    @Override
    public int compareTo(Event event) {
        return startEvent.compareTo(event.startEvent);
    }
}
