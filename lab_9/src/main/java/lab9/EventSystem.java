package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Class representing event managing system
 */
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Method yang digunakan untuk mencari User yang telah diadd kedalam ArrayList users di EventSystem
     * @param name mencari User yang mempunyai nama yang equal
     * @return object User jika ada, null jika tidak ada
     */
    public User getUser(String name) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getName().equals(name)) {
                return users.get(i);
            }
        }
        return null;
    }

    /**
     * Method yang digunakan untuk mencari Event yang telah diadd kedalam ArrayList events di EventSystem
     * @param name mencari Event yang mempunyai nama yang equal
     * @return object Event jika ada, null jika tidak ada
     */
    public Event getEvent(String name) {
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getName().equals(name)) {
                return events.get(i);
            }
        }
        return null;
    }

    /**
     * Method ini digunakan untuk menambahkan sebuah acara ke dalam sistem.
     * Pengguna hanya boleh mendaftar ke acara yang terdaftar.
     *
     * @param name Seperti user, tidak boleh ada beberapa acara dengan nama sama.
     *             Acara yang akan ditambahkan namun namanya sudah terdaftar akan ditolak.
     *             startTimeStr & endTimeStr merupakan representasi String dari waktu mulai dan selesai acara.
     *             String ini berbentuk "yyyy-MM-dd_HH:mm:ss" (tahun-bulan-hari_jam:menit:detik).
     *             costPerHourStr    String yang merepresentasikan biaya yang perlu dibayar pengguna yang menghadiri acara.
     * @return String   dengan ketentuan:
     * Bila waktu mulai > waktu selesai, kembalikan String Waktu yang diinputkan tidak valid!
     * <p>
     * Bila acara berhasil ditambahkan, kembalikan String Event eventName berhasil ditambahkan!
     * (gantikan "eventName" dengan nama acara yang ditambahkan).
     * <p>
     * Bila acara gagal ditambahkan (karena sudah ada acara dengan nama sama),
     * kembalikan String Event eventName sudah ada! (gantikan "eventName" dengan nama acara yang ditambahkan).
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String cost) {
        Event eventAvail = getEvent(name);
        if (eventAvail == null) {
            String[] startDateList = startTimeStr.split("_")[0].split("-");
            String[] startTimeList = startTimeStr.split("_")[1].split(":");
            String[] endDateList = endTimeStr.split("_")[0].split("-");
            String[] endTimeList = endTimeStr.split("_")[1].split(":");
            GregorianCalendar startEvent = new GregorianCalendar(Integer.parseInt(startDateList[0]),
                    Integer.parseInt(startDateList[1]) - 1, Integer.parseInt(startDateList[2]),
                    Integer.parseInt(startTimeList[0]), Integer.parseInt(startTimeList[1]),
                    Integer.parseInt(startTimeList[2]));
            GregorianCalendar endEvent = new GregorianCalendar(Integer.parseInt(endDateList[0]),
                    Integer.parseInt(endDateList[1]) - 1, Integer.parseInt(endDateList[2]),
                    Integer.parseInt(endTimeList[0]), Integer.parseInt(endTimeList[1]),
                    Integer.parseInt(endTimeList[2]));
            if (startEvent.compareTo(endEvent) > 0) {
                return "Waktu yang diinputkan tidak valid!";
            }
            BigInteger costInt = new BigInteger(cost);
            events.add(new Event(name, startEvent, endEvent, costInt));
            return "Event " + name + " berhasil ditambahkan!";
        } else {
            return "Event " + name + " sudah ada!";
        }
    }

    /**
     * Method ini digunakan untuk menambahkan seorang pengguna ke dalam sistem.
     * Hanya pengguna yang terdaftar dalam sistem yang boleh mendaftar pada suatu acara.
     * Catatan: Sistem ini masih sangat baru sehingga belum dapat menggunakan KTM/KTP untuk identifikasi.
     * Dalam sistem, tidak boleh ada 2 atau lebih pengguna dengan nama sama.
     * Pengguna yang mendaftar namun namanya sudah terdaftar akan ditolak.
     *
     * @param name nama dari User
     * @return String   dengan ketentuan:
     * Bila pengguna berhasil ditambahkan, kembalikan String User userName berhasil ditambahkan!
     * (gantikan "userName" dengan nama user yang ditambahkan).
     * <p>
     * Bila pengguna gagal ditambahkan (karena sudah ada pengguna dengan nama sama),
     * kembalikan String User username sudah ada! (gantikan "userName" dengan nama user yang ditambahkan).
     */
    public String addUser(String name) {
        // TODO: Implement:
        User userAvail = getUser(name);
        if (userAvail == null) {
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        } else {
            return "User " + name + " sudah ada!";
        }
    }

    /**
     * Mengatur agar pengguna dengan nama userName berencana menghadiri acara eventName.
     * Penambahan rencana ini berhasil jika pengguna tidak memiliki acara
     * yang waktunya bertabrakan dengan acara yang akan ditambahkan.
     *
     * @param userName nama dari User yang akan diregister ke Event
     *                 eventName name dari Event yang akan diregister oleh User
     * @return String   dengan ketentuan:
     * Bila pengguna dan acara yang dicari tidak ada, kembalikan String Tidak ada pengguna dengan nama userName
     * dan acara dengan nama eventName! (gantikan "userName" dengan nama user dan "eventName" dengan nama acara).
     * <p>
     * Bila tidak ada pengguna dengan nama userName, kembalikan String
     * Tidak ada pengguna dengan nama userName! (gantikan "userName" dengan nama user yang dicari).
     * <p>
     * Bila tidak ada acara dengan nama eventName, kembalikan String
     * Tidak ada acara dengan nama eventName! (gantikan "eventName" dengan nama user yang dicari).
     * <p>
     * Bila pengguna sudah berencana menghadiri acara yang akan bertabrakan dengan event yang akan ditambahkan,
     * kembalikan String userName sibuk sehingga tidak dapat menghadiri eventName!
     * (gantikan "userName" dengan nama user dan "eventName" dengan nama acara).
     * <p>
     * Bila pengguna berhasil menambahkan acara ke list acara yang akan dihadiri, kembalikan String
     * userName berencana menghadiri eventName!
     * (gantikan "userName" dengan nama user dan "eventName" dengan nama acara).
     */
    public String registerToEvent(String userName, String eventName) {
        // TODO: Implement
        User userAvail = getUser(userName);
        Event eventAvail = getEvent(eventName);
        if (userAvail == null && eventAvail == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (userAvail == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (eventAvail == null) {
            return "Tidak ada acara dengan nama" + eventName + "!";
        } else {
            if (userAvail.addEvent(eventAvail)) {
                return userName + " berencana menghadiri " + eventName + "!";
            } else {
                return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
            }
        }
    }
}