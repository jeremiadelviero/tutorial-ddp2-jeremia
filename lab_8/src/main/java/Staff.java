public class Staff extends Karyawan{
    private static int BATASGAJI;

    public Staff(String name, int gajiAwal) {
        super(name, "Staff", gajiAwal);
    }

    public static void setBATASGAJI(int batasgaji) {
        BATASGAJI = batasgaji;
    }

    public static int getBATASGAJI() {
        return BATASGAJI;
    }
}