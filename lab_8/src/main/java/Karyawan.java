import java.util.ArrayList;

public class Karyawan {
    private static final int MAXBAWAHAN = 10;
    private String name;
    private String type;
    private int gaji;
    private int gajianCounter;
    private ArrayList<Karyawan> paraBawahan = new ArrayList<Karyawan>();

    public Karyawan(String name, String type, int gajiAwal) {
        this.name = name;
        this.type = type;
        this.gaji = gajiAwal;
    }

    public Karyawan findBawahan (String name) {
        for (int i = 0; i < paraBawahan.size(); i++) {
            if (paraBawahan.get(i).getName().equals(name)) {
                return paraBawahan.get(i);
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public void gajian() {
        this.gajianCounter++;
        int gajiLama = 0;
        if (this.gajianCounter % 6 == 0 && gajianCounter != 0) {
            gajiLama = this.gaji;
            this.gaji += this.gaji / 10;
            System.out.printf("%s mengalami kenaikan gaji sebesar 10%% dari %d menjadi %d\n",
                    this.name, gajiLama, this.gaji);
        }
    }

    public void tambahBawahan (Karyawan bawahan) {
        if (findBawahan(bawahan.getName()) == null) {
            if (this.paraBawahan.size() > MAXBAWAHAN) {
                System.out.println("Anda tidak layak memiliki bawahan");
            } else {
                this.paraBawahan.add(bawahan);
                System.out.printf("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n",
                        bawahan.getName(), this.name);
            }
        } else {
            System.out.printf("Karyawan %s telah menjadi bawahan %s\n",
                    bawahan.getName(), this.name);
        }
    }
}