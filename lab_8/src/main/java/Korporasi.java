import java.util.ArrayList;

public class Korporasi {
    private static final int MAXKARYAWAN = 10000;
    private String name;
    private ArrayList<Karyawan> semuaKaryawan = new ArrayList<Karyawan>();

    public Korporasi(String name) {
        this.name = name;
    }

    public Karyawan findKaryawan (String name) {
        for (int i = 0; i < semuaKaryawan.size(); i++) {
            if (semuaKaryawan.get(i).getName().equals(name)) {
                return semuaKaryawan.get(i);
            }
        }
        return null;
    }

    public void add (Karyawan karyawan) {
        if (findKaryawan(karyawan.getName()) == null && semuaKaryawan.size() < MAXKARYAWAN) {
            semuaKaryawan.add(karyawan);
            System.out.printf("%s mulai bekerja sebagai %s di %s\n",
                    karyawan.getName(), karyawan.getType(), this.name);
        } else if (semuaKaryawan.size() >= MAXKARYAWAN) {
            System.out.printf("%s sudah mempunyai maksimum karyawan", this.name);
        } else {
            System.out.printf("Karyawan dengan nama %s telah terdaftar\n", karyawan.getName());
        }
    }
    public void status (String name) {
        Karyawan temp = findKaryawan(name);
        if (temp != null) {
            System.out.printf("%s %d\n", temp.getName(), temp.getGaji());
        } else {
            System.out.println("Karyawan tidak ditemukan");
        }
    }
    
    public void gajian () {
        System.out.println("Semua karyawan telah diberikan gaji");
        for (int i = 0; i < semuaKaryawan.size(); i++) {
            Karyawan karyawan = semuaKaryawan.get(i);
            karyawan.gajian();
            if (karyawan.getType().equalsIgnoreCase("STAFF")) {
                if (karyawan.getGaji() > ((Staff)karyawan).getBATASGAJI()) {
                    System.out.printf("Selamat, %s telah dipromosikan menjadi MANAGER\n",
                            karyawan.getName());
                    Karyawan naikJabatan = new Manager(karyawan.getName(), karyawan.getGaji());
                    semuaKaryawan.remove(karyawan);
                    semuaKaryawan.add(naikJabatan);
                }
            }
        }
    }

    public void tambahBawahan (String atasan, String bawahan) {
        Karyawan tempAtasan = findKaryawan(atasan);
        Karyawan tempBawahan = findKaryawan(bawahan);
        if (tempAtasan != null && tempBawahan != null) {
            if (tempAtasan.getType().equalsIgnoreCase("MANAGER")) {
                if (tempBawahan.getType().equalsIgnoreCase("STAFF") ||
                        tempBawahan.getType().equalsIgnoreCase("INTERN")) {
                    tempAtasan.tambahBawahan(tempBawahan);
                } else {
                    System.out.println("Anda tidak layak memiliki bawahan");
                }
            } else if (tempAtasan.getType().equalsIgnoreCase("STAFF")) {
                if (tempBawahan.getType().equalsIgnoreCase("INTERN")) {
                    tempAtasan.tambahBawahan(tempBawahan);
                } else {
                    System.out.println("Anda tidak layak memiliki bawahan");
                }
            } else if (tempAtasan.getType().equalsIgnoreCase("INTERN")) {
                System.out.println("Anda tidak layak memiliki bawahan");
            }
        } else {
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }

    public static int getMAXKARYAWAN() {
        return MAXKARYAWAN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Karyawan> getSemuaKaryawan() {
        return semuaKaryawan;
    }

    public void setSemuaKaryawan(ArrayList<Karyawan> semuaKaryawan) {
        this.semuaKaryawan = semuaKaryawan;
    }
}