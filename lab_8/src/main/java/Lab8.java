import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int batasgaji = sc.nextInt();
        Staff.setBATASGAJI(batasgaji);
        System.out.printf("Batas gaji Staff sebesar %d\n", batasgaji);

        execCommand();
    }

    public static void execCommand() {
        Scanner sc = new Scanner(System.in);
        Korporasi ptTampan = new Korporasi("PT. TAMPAN");
        while (true) {
            String command = sc.nextLine();

            String[] commandList = command.split(" ");

            if (commandList[0].equalsIgnoreCase("TAMBAH_KARYAWAN")) {
                if (commandList[1].equalsIgnoreCase("MANAGER")) {
                    Karyawan temp = new Manager(commandList[2],
                            Integer.parseInt(commandList[3]));
                    ptTampan.add(temp);
                } else if (commandList[1].equalsIgnoreCase("STAFF")) {
                    Karyawan temp = new Staff(commandList[2],
                            Integer.parseInt(commandList[3]));
                    ptTampan.add(temp);
                } else if (commandList[1].equalsIgnoreCase("INTERN")) {
                    Karyawan temp = new Intern(commandList[2],
                            Integer.parseInt(commandList[3]));
                    ptTampan.add(temp);
                } else {
                    System.out.println("Posisi tersebut tidak ada");
                }

            } else if (commandList[0].equalsIgnoreCase("STATUS")) {
                ptTampan.status(commandList[1]);

            } else if (commandList[0].equalsIgnoreCase("TAMBAH_BAWAHAN")) {
                ptTampan.tambahBawahan(commandList[1], commandList[2]);

            } else if (commandList[0].equalsIgnoreCase("GAJIAN")) {
                ptTampan.gajian();
            } else {
                System.out.println("Input salah");
            }
        }
    }
}
