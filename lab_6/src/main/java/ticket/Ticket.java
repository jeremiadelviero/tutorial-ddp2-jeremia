package ticket;
import movie.Movie;

public class Ticket {
	private Movie film;
	private String jadwalHari;
	private boolean is3D = false;

	public Ticket(Movie film, String jadwalHari, boolean is3D) {
		this.film = film;
		this.jadwalHari = jadwalHari;
		this.is3D = is3D;
	}

	public Movie getFilm() {
		return film;
	}

	public String cek3D() {
        if (this.is3D) {
            return "3 Dimensi";
        } else {
            return "Biasa";
        }
    }

    public String getJadwalHari() {
    	return jadwalHari;
    }

    public int getHarga() {
    	int harga = 60000;
    	if (this.jadwalHari.equals("Sabtu") || this.jadwalHari.equals("Minggu")) {
    		harga += 40000;
    	}
    	if (this.is3D) {
    		harga += harga/5;
    	}
    	return harga;
    }
}