package movie;

public class Movie {
	private String judul;
	private String rating;
	private int durasi;
	private String genre;
	private String jenis;
	private int batasUmur;

	public Movie(String judul, String rating, int durasi, String genre, String jenis) {
		this.judul = judul;
		this.rating = rating;
		if (this.rating.equals("Umum")) {
			this.batasUmur = 0;
		} else if (this.rating.equals("Remaja")) {
			this.batasUmur = 13;
		} else if (this.rating.equals("Dewasa")) {
			this.batasUmur = 17;
		}
		this.durasi = durasi;
		this.genre = genre;
		this.jenis = jenis;
	}

	public String getJudul() {
		return judul;
	}

	public String getRating() {
		return rating;
	}

	public int getDurasi() {
		return durasi;
	}

	public String getGenre() {
		return genre;
	}

	public String getJenis() {
		return jenis;
	}

	public int getBatasUmur() {
		return batasUmur;
	}
}