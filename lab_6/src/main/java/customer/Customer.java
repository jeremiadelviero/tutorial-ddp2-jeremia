package customer;
import java.util.ArrayList;
import theater.Theater;
import ticket.Ticket;
import movie.Movie;

public class Customer {
	private String nama;
	private String jenisKelamin;
	private int umur;

	public Customer (String nama, String jenisKelamin, int umur) {
		this.nama = nama;
		this.jenisKelamin = jenisKelamin;
		this.umur = umur;
	}

	public Ticket orderTicket(Theater theater, String judul, String jadwalHari, String jenisFilm) {
		int counter = 0;
        for (Ticket ticket : theater.getListTicket()) {
            if (ticket.getFilm().getJudul().equals(judul) && ticket.getJadwalHari().equals(jadwalHari)
                    && ticket.cek3D().equals(jenisFilm)) {
                if (umur >= ticket.getFilm().getBatasUmur()) {
                    System.out.printf("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d\n", nama, judul, jenisFilm, theater.getNamaBioskop(), jadwalHari, ticket.getHarga());
                    theater.tambahSaldo(ticket.getHarga());
                    return ticket;
                } else {
                    System.out.printf("%s masih belum cukup umur untuk menonton %s dengan rating %s\n", nama, judul, ticket.getFilm().getRating());
                }
            } else {
                counter += 1;
                if (counter == theater.getListTicket().size()) {
                    System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n", judul, jenisFilm, jadwalHari, theater.getNamaBioskop());
                }
            }

        }
        return null;
	}
				

	public void findMovie(Theater theater, String judul) {
		Movie[] listFilm = theater.getListFilm();
		for (int i = 0; i < listFilm.length; i++) {
			if (listFilm[i].getJudul().equals(judul)) {
				System.out.printf(("------------------------------------------------------------------\n"
				+ "Judul   : %s\n"
				+ "Genre   : %s\n"
				+ "Durasi  : %d menit\n"
				+ "Rating  : %s\n"
				+ "Jenis   : Film %s\n"
				+ "------------------------------------------------------------------\n"), listFilm[i].getJudul(), listFilm[i].getGenre(),
				listFilm[i].getDurasi(), listFilm[i].getRating(), listFilm[i].getJenis());
			} else {
				if (i == listFilm.length - 1) {
					System.out.printf("Film %s yang dicari %s tidak ada di bioskop %s\n", judul, this.nama, theater.getNamaBioskop());
				}
			}
		}
	}
}	