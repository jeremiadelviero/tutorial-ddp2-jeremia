package theater;
import java.util.ArrayList;
import movie.Movie;
import theater.Theater;
import ticket.Ticket;

public class Theater {
	private static Theater[] listBioskop = new Theater[10]; 
	private String namaBioskop;
	private int saldo;
	private ArrayList<Ticket> listTicket = new ArrayList<Ticket>();
	private Movie[] listFilm;

	public Theater (String namaBioskop, int saldoAwal, ArrayList<Ticket> ticketAwal, Movie[] listFilm) {
		this.namaBioskop = namaBioskop;
		this.saldo = saldoAwal;
		this.listTicket = ticketAwal;
		this.listFilm = listFilm;
		addToListBioskop(this);
	}

	public static void addToListBioskop(Theater theater) {
		for (int i = 0; i < 10; i++) {
			if (listBioskop[i] == null) {
				listBioskop[i] = theater;
				break;
			}
		}
	}

	public void printInfo() {
		String daftarFilm = "";
		for (int i = 0; i < listFilm.length; i++) {
			if (i != listFilm.length - 1) {
				daftarFilm += listFilm[i].getJudul() + ", ";
			} else {
				daftarFilm += listFilm[i].getJudul();
			}
		}
		System.out.printf(("------------------------------------------------------------------\n"
			+ "Bioskop                 : %s\n"
			+ "Saldo Kas               : %d\n"
			+ "Jumlah tiket tersedia   : %d\n"
			+ "Daftar Film tersedia    : %s\n"
			+ "------------------------------------------------------------------\n"), this.namaBioskop, this.saldo, this.listTicket.size(), daftarFilm);
	}

	public static void printTotalRevenueEarned(Theater[] listBioskop) {
		int totalSaldo = 0;
		for (Theater t : listBioskop) {
			totalSaldo += t.getSaldo();
		}
		System.out.printf("Total uang yang dimiliki Koh Mas : Rp. %d\n", totalSaldo);
		System.out.println("------------------------------------------------------------------");
		for (Theater t : listBioskop) {
			System.out.printf(("Bioskop     : %s\n"
				+ "Saldo Kas   : Rp. %d\n\n"),t.getNamaBioskop(), t.getSaldo());
		}
		System.out.println("------------------------------------------------------------------");
	}

   	public String getNamaBioskop() {
   		return namaBioskop;
   	}

	public Movie[] getListFilm() {
		return listFilm;
	}

	public void setListTicket(ArrayList<Ticket> listTicket) {
		this.listTicket = listTicket;
	}
	public ArrayList<Ticket> getListTicket() {
		return listTicket;
	}

	public void tambahSaldo(int saldo) {
		this.saldo += saldo;
	}
	public int getSaldo() {
		return saldo;
	}
}