import java.util.ArrayList;
public class Manusia {
	// Atribut dari kelas
	public static ArrayList<Manusia> arrayManusia = new ArrayList<Manusia>();
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	private boolean meninggal;
	private Manusia objek;
	
	// Konstruktor kelas Manusia
	public Manusia(String nama, int umur) {
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
		this.meninggal = false;
		this.objek = this;
		arrayManusia.add(this.objek);
	}
	public Manusia(String nama, int umur, int uang) {
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
		this.meninggal = false;
		this.objek = this;
		arrayManusia.add(this.objek);
	}
	
	// Setters dan Getters
	public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNama() {
        return nama;
    }
	
	public void setUmur(int umur) {
        this.umur = umur;
    }
    public int getUmur() {
        return umur;
    }
	
	public void setUang(int uang) {
        this.uang = uang;
    }
    public int getUang() {
        return uang;
    }
	
	public void setKebahagiaan(float kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }
    public float getKebahagiaan() {
        return kebahagiaan;
    }
	
	public void setMeninggal(boolean meninggal) {
        this.meninggal = meninggal;
    }
    public boolean getMeninggal() {
        return meninggal;
    }
	
	public void setObjek(Manusia objek) {
        this.objek = objek;
    }
    public Manusia getObjek() {
        return objek;
    }
	
	
	public void beriUang(Manusia penerima) {
		int hasil = 0;
		if (this.meninggal) {
			System.out.println(this.nama + " telah tiada");
		} else {
			if (penerima.getMeninggal()) {
				System.out.println(penerima.getNama() + " telah tiada");
			} else {
				String namaPenerima = penerima.getNama();
				for (int i = 0; i < namaPenerima.length(); i++) {
					char character = namaPenerima.charAt(i);
					int charAscii = (int) character;
					hasil += charAscii;
				}
				int jumlahUang = hasil * 100;
				if (this.uang > jumlahUang) {
					this.uang -= jumlahUang;
					penerima.setUang(penerima.getUang() + jumlahUang);
					this.kebahagiaan += ((float)jumlahUang / 6000);
					penerima.setKebahagiaan(penerima.getKebahagiaan() + ((float)jumlahUang / 6000));
					if (this.kebahagiaan > 100) {
						this.kebahagiaan = 100;
					}
					if (penerima.getKebahagiaan() > 100) {
						penerima.setKebahagiaan(100);
					}
					System.out.println(this.nama + " memberi uang sebanyak " + jumlahUang +
									   " kepada " + namaPenerima + ", mereka berdua senang :D");
				} else {
					System.out.println(this.nama + " ingin memberi uang kepada " + namaPenerima +
									   " namun tidak memiliki cukup uang :'(");
				}
			}
		}
	}
	
	
	public void beriUang(Manusia penerima, int jumlah) {
		if (this.meninggal) {
			System.out.println(this.nama + " telah tiada");
		} else {
			if (penerima.getMeninggal()) {
				System.out.println(penerima.getNama() + " telah tiada");
			} else {
				String namaPenerima = penerima.getNama();
				int jumlahUang = jumlah;
				if (this.uang > jumlahUang) {
					this.uang -= jumlahUang;
					penerima.setUang(penerima.getUang() + jumlahUang);
					this.kebahagiaan += ((float)jumlahUang / 6000);
					penerima.setKebahagiaan(penerima.getKebahagiaan() + ((float)jumlahUang / 6000));
					if (this.kebahagiaan > 100) {
						this.kebahagiaan = 100;
					}
					if (penerima.getKebahagiaan() > 100) {
						penerima.setKebahagiaan(100);
					}
					System.out.println(this.nama + " memberi uang sebanyak " + jumlahUang +
									   " kepada " + namaPenerima + ", mereka berdua senang :D");
				} else {
					System.out.println(this.nama + " ingin memberi uang kepada " + namaPenerima +
									   " namun tidak memiliki cukup uang :'(");
				}
			}
		}
			
	}
	
	public void bekerja(int durasi, int bebanKerja) {
		int pendapatan = 0;
		int bebanKerjaTotal = 0;
		int durasiBaru = 0;
		if (this.meninggal) {
			System.out.println(this.nama + " telah tiada");
		} else {
			if (this.umur < 18) {
				System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
			} else {
				bebanKerjaTotal = durasi * bebanKerja;
				if (bebanKerjaTotal <= this.kebahagiaan) {
					this.kebahagiaan -= bebanKerjaTotal;
					pendapatan = bebanKerjaTotal * 10000;
					System.out.println(this.nama + " bekerja full time, total pendapatan : " +
									   pendapatan);				
				} else {
					durasiBaru = (int) this.kebahagiaan / bebanKerja;
					bebanKerjaTotal = durasiBaru * bebanKerja;
					if (this.kebahagiaan - bebanKerjaTotal < 0) {
						bebanKerjaTotal = (int) this.kebahagiaan;
						this.kebahagiaan = 0;
					} else {
						this.kebahagiaan -= bebanKerjaTotal;
					}
					pendapatan = bebanKerjaTotal * 10000;
					System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "
									   + pendapatan);	
				}
				this.uang += pendapatan;
			}
		}
	}
	
	public void rekreasi(String namaTempat) {
		int biaya = namaTempat.length() * 10000;
		if (this.meninggal) {
			System.out.println(this.nama + " telah tiada");
		} else {
			if (this.uang > biaya) {
				this.uang -= biaya;
				this.kebahagiaan += namaTempat.length();
				if (this.kebahagiaan > 100) {
					this.kebahagiaan = 100;
				}
				System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang :)");
			} else {
				System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
			
			}
		}
	}
	
	public void sakit(String namaPenyakit) {
		if (this.meninggal) {
			System.out.println(this.nama + " telah tiada");
		} else {
			this.kebahagiaan -= namaPenyakit.length();
			if (this.kebahagiaan < 0) {
				this.kebahagiaan = 0;
			}
			System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O");
		}
	}
	
	public void meninggal() {
		if (this.meninggal) {
			System.out.println(this.nama + " telah tiada");
		} else {
			System.out.println(this.nama + " meninggal dengan tenang, kebahagiaan : " + this.kebahagiaan);
			if (Manusia.arrayManusia.size() > 1) {
				arrayManusia.remove(this.objek);
				Manusia penerima = arrayManusia.get(arrayManusia.size() - 1);
				penerima.setUang(penerima.getUang() + this.uang);
				this.uang = 0;
				System.out.println("Semua harta " + this.nama + " disumbangkan untuk " + penerima.getNama());	
			} else {
				arrayManusia.remove(this.objek);
				this.uang = 0;
				System.out.println("Semua harta " + this.nama + " hangus");
			}
			this.nama = "Almarhum " + this.nama;
			this.meninggal = true;
		}
	}
	
	public String toString() {
		return "Nama\t\t: " + this.nama + "\n" +
			   "Umur\t\t: " + this.umur + "\n" +
			   "Uang\t\t: " + this.uang + "\n" +
			   "Kebahagiaan\t: " + this.kebahagiaan;
	}
	
}

	
	